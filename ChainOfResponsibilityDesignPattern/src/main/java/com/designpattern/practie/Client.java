package com.designpattern.practie;

public class Client {
    ErrorHandler errorHandler1;
    public Client(){
        errorHandler1 = new FaxErrorHandler();

        ErrorHandler errorHandler2 = new EmailErrorHandler();
        ErrorHandler errorHandler3= new AuthenticationErrorHandler();
        ErrorHandler errorHandler4 = new otherErrorHandler();
        errorHandler1.setNextErrorHandler(errorHandler2);
        errorHandler2.setNextErrorHandler(errorHandler3);
        errorHandler3.setNextErrorHandler(errorHandler4);
    }

    public static void main(String[] args) {
        try {
            Client errorClient = new Client();

            Message message = new ErrorMessage();

            //Handle Emil error
            String errorMessage = message.createErrorMessage("Caught Email error", MessageEnum.HIGH);
            errorClient.errorHandler1.handleError(errorMessage);

            //Handle Fax error
            errorMessage = message.createErrorMessage("Caught Fax error", MessageEnum.MODERATE);
            errorClient.errorHandler1.handleError(errorMessage);

            //Handle Authentication error
            errorMessage = message.createErrorMessage("Caught User error", MessageEnum.LOW);
            errorClient.errorHandler1.handleError(errorMessage);

            //Handle other error
            errorMessage = message.createErrorMessage("Caught Other error", MessageEnum.LOW);
            errorClient.errorHandler1.handleError(errorMessage);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
