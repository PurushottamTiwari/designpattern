package com.designpattern.practie;

public interface ErrorHandler {
    void setNextErrorHandler(ErrorHandler errorHandler);
    void handleError(String errorMessage) ;
}

class EmailErrorHandler implements ErrorHandler{
    ErrorHandler errorHandler = null;

    @Override
    public void setNextErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    @Override
    public void handleError(String errorMessage)  {
         if(errorMessage.contains("Email") || errorMessage.contains("email")) {
             System.out.println("Email Error Handled");
         }else{
             if(errorHandler != null) errorHandler.handleError(errorMessage);
         }
    }
}
class FaxErrorHandler implements ErrorHandler{
    ErrorHandler errorHandler = null;
    @Override
    public void setNextErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    @Override
    public void handleError(String errorMessage)  {
        if(errorMessage.contains("Fax") || errorMessage.contains("fax")) {
            System.out.println("Fax Error Handled");
        }else{
           if(errorHandler != null) errorHandler.handleError(errorMessage);
        }
    }
}
class AuthenticationErrorHandler implements ErrorHandler{
    ErrorHandler errorHandler = null;
    @Override
    public void setNextErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    @Override
    public void handleError(String errorMessage)  {
        if(errorMessage.contains("User") || errorMessage.contains("user")) {
            System.out.println("Authentiation Error Handled");
        }else{
            if(errorHandler != null) errorHandler.handleError(errorMessage);
        }
    }
}
class otherErrorHandler implements ErrorHandler{
    ErrorHandler errorHandler = null;
    @Override
    public void setNextErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    @Override
    public void handleError(String errorMessage) {
        System.out.println("Other Error Handled");
    }
}