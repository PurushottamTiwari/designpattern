package com.designpattern.practie;

public interface Message {
    String createErrorMessage(String msg, MessageEnum messageEnum);
}

class ErrorMessage implements Message{

    @Override
    public String createErrorMessage(String msg , MessageEnum messageEnum) {
        return new StringBuilder().append(msg).append(messageEnum).toString();
    }
}
