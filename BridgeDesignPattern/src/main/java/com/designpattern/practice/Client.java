package com.designpattern.practice;

public class Client {
    public static void main(String[] args) {
        Color color = new RedColor();
        Shape shape = new Triangle(color);
        shape.drwShape();

        Color color1 = new YellowColor();
        Shape shape1 = new Rectangle(color1);
        shape1.drwShape();

        Color color2 = new RedColor();
        Shape shape2 = new Circle(color2);
        shape2.drwShape();
    }
}
