package com.designpattern.practice;

abstract class Shape {
    protected Color color;
    abstract void drwShape();

}
class Triangle extends Shape{

    public Triangle(Color color) {
        this.color = color;
    }

    public void drwShape() {
        System.out.println("Triangle drawn");
        color.fillColor();

    }
}

class Rectangle extends Shape{
    public Rectangle(Color color) {
        this.color = color;
    }

    public void drwShape() {
        System.out.println("Rectangle  drawn");
        color.fillColor();

    }
}
class Circle extends Shape{
    public Circle(Color color) {
        this.color = color;
    }

    @Override
    void drwShape() {
        System.out.println("Circle  drawn");
        color.fillColor();
    }
}
