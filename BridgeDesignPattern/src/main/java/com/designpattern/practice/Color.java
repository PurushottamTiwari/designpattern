package com.designpattern.practice;

interface Color {
    void fillColor();
}
class RedColor implements Color{

    @Override
    public void fillColor() {
        System.out.println("Filled with Red Color");
    }
}
class GreenColor implements Color{

    @Override
    public void fillColor() {
        System.out.println("Filled with Green Color");
    }
}
class YellowColor implements Color{

    @Override
    public void fillColor() {
        System.out.println("Filled with Yello Color");
    }
}