package com.designpattern.practice;

public interface Expression {
    int interpret(InterpreterContext interpreterContext);
}

class AdditionExpression implements Expression{
    int a, b;

    public AdditionExpression(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public int interpret(InterpreterContext interpreterContext) {
        return interpreterContext.add(a,b);
    }
}

class SubstractionExpression implements Expression{

    int a,b;

    public SubstractionExpression(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public int interpret(InterpreterContext interpreterContext) {
        return interpreterContext.minus(a,b);
    }
}
