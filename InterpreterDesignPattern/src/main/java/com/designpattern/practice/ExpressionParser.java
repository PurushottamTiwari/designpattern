package com.designpattern.practice;

import java.util.Locale;

public class ExpressionParser {
    InterpreterContext interpreterContext;
    public ExpressionParser(InterpreterContext interpreterContext){
        this.interpreterContext = interpreterContext;
    }

    public int parseExpression(String expression){
        Expression exp;

        int num1 = Integer.parseInt(expression.substring(0, expression.indexOf(" ")));
        int num2 = Integer.parseInt(expression.substring(expression.lastIndexOf(" ")+1));
        if(expression.toLowerCase().contains("plus")){
            exp = new AdditionExpression(num1,num2);
            return exp.interpret(interpreterContext);
        }
        if(expression.toLowerCase().contains("minus")){
            exp = new SubstractionExpression(num1, num2);
            return exp.interpret(interpreterContext);
        }

        return 0;
    }
}
