package com.designpattern.practice;

public class Client {
    public static void main(String[] args) {
        InterpreterContext interpreterContext = new InterpreterContext();
        ExpressionParser expressionParser  =new ExpressionParser(interpreterContext);

        System.out.println("2 PLUS 3" +"="+expressionParser.parseExpression("2 PLUS 3"));


        System.out.println("2 MINUS 3" +"="+expressionParser.parseExpression("2 MINUS 3"));

    }
}
