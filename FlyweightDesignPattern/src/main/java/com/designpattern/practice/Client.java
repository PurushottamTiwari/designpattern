package com.designpattern.practice;

public class Client {
    public static void main(String[] args) {
        StudentFinder studentFinder = new StudentFinder();
         for(int i = 0 ; i < 5 ;i ++){
             Student student = studentFinder.getStudent("Primary","Mukund",8);
             System.out.println("HashCode :"+student.hashCode());
             System.out.println(student.showDetails());
         }
        System.out.println("********************************************************************************************");
        for(int i = 0 ; i < 5 ;i ++){
            Student student = studentFinder.getStudent("Higher","Purushottam",44);
            System.out.println("HashCode :"+student.hashCode());
            System.out.println(student.showDetails());
        }
    }
}
