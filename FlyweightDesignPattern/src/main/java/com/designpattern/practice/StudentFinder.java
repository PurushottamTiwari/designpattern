package com.designpattern.practice;

import java.util.Hashtable;

public class StudentFinder {
     Hashtable<String, Student> studentHashtable = new Hashtable<>();

    public  Student getStudent(String studentType, String studentName, int rollNo){
        if(studentHashtable.containsKey(studentType)) return studentHashtable.get(studentType);

        switch(studentType){
            case "Primary" :
                Student student1 = new PrimarySchoolStudent(studentName, rollNo);
                studentHashtable.put(studentType,student1);
                return student1;
            case "Higher":
                Student student2 = new HighSchoolStudent(studentName,rollNo);
                studentHashtable.put(studentType,student2);
                return  student2;
        }
       return null;
    }
}
