package com.designpattern.practice;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

abstract class Student {
    protected String studentName;
    protected int rollNo;
    public Student(String studentName, int rollNo) {
        this.studentName = studentName;
        this.rollNo = rollNo;
    }
   abstract String showDetails();
}


class HighSchoolStudent extends Student{

    public HighSchoolStudent(String studentName, int rollNo) {
        super(studentName, rollNo);
    }

    @Override
    String  showDetails() {
        return toString();
    }

    @Override
    public String toString() {
        return "HighSchoolStudent{" +
                "studentName='" + studentName + '\'' +
                ", rollNo=" + rollNo +
                '}';
    }
}
class PrimarySchoolStudent extends Student{

    public PrimarySchoolStudent(String studentName, int rollNo) {
        super(studentName, rollNo);
    }

    @Override
    String  showDetails() {
        return toString();
    }

    @Override
    public String toString() {
        return "PrimarySchoolStudent{" +
                "studentName='" + studentName + '\'' +
                ", rollNo=" + rollNo +
                '}';
    }
}