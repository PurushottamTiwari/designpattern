package practice;

public interface Builder {

    UserBuilder setAge(int age);
    UserBuilder setFirstName(String firstName);
    UserBuilder setLastName(String lastName);
    UserBuilder setPhone(String phone);
    UserBuilder setAddress(String address);
    User build();
}

class UserBuilder implements Builder{
    private  String firstName;
    private  String lastName;
    private int age;
    private String phone;
    private String address;


    @Override
    public UserBuilder setAge(int age) {
        this.age = age;
        return this;
    }

    @Override
    public UserBuilder setFirstName(String firstName) {
       this.firstName = firstName;
       return this;
    }

    @Override
    public UserBuilder setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    @Override
    public UserBuilder setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    @Override
    public UserBuilder setAddress(String address) {
       this.address = address;
       return this;
    }

    @Override
    public User build() {
        User user = new User(this);
        return user;
    }

    public String getFirstName() {
        return firstName;
    }


    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }
}
