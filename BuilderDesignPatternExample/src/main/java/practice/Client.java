package practice;

public class Client {
    public static void main(String[] args) {
        Builder builder = new UserBuilder();
        User user1 = builder.setFirstName("Purushottam").setLastName("Tiwari").build();
        System.out.println(user1);

        User user2 = builder.setFirstName("Purushottam").setLastName("Tiwari").setAge(58).build();
        System.out.println(user2);

        User user3 = builder.setFirstName("Purushottam").setLastName("Tiwari").setAge(58).setPhone("123456789").setAddress("Pune").build();
        System.out.println(user3);
    }
}
