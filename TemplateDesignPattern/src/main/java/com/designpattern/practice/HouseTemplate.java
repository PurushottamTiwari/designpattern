package com.designpattern.practice;

abstract class HouseTemplate {

    public final void buildHouse(){
        buildFoundation();
        buildPillars();
        buildWalls();
        buildDoordsAndWindows();
        buildRoof();
        finalMessage();
    }
    private void buildFoundation(){
        System.out.println("Build the Foundation");
    }
    private void buildPillars(){
        System.out.println("Build Pillars");
    }
    public abstract void buildDoordsAndWindows();
    public abstract void buildWalls();
    public abstract void buildRoof();
    public abstract void finalMessage();
}
 class  CementHouse extends HouseTemplate{

     @Override
     public void buildDoordsAndWindows() {
         System.out.println("Build Dorrs and Windows of Glass");
     }

     @Override
     public void buildWalls() {
         System.out.println("Build Walls of Cement");
     }

     @Override
     public void buildRoof() {
         System.out.println("Build roof of cement");
     }
     @Override
     public void finalMessage(){
         System.out.println("Cement House Built");
     }
 }

class  ClayHouse extends HouseTemplate{

    @Override
    public void buildDoordsAndWindows() {
        System.out.println("Build Dorrs and Windows of Wood");
    }

    @Override
    public void buildWalls() {
        System.out.println("Build Walls of Clay");
    }

    @Override
    public void buildRoof() {
        System.out.println("Build roof of Clay");
    }

    @Override
    public void finalMessage(){
        System.out.println("Clay House Built");
    }
}
