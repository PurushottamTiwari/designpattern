package com.designpattern.practice;

public class Client {
    public static void main(String[] args) {
        HouseTemplate houseTemplate = new CementHouse();
        houseTemplate.buildHouse();

        System.out.println("************************************************");

        houseTemplate = new ClayHouse();
        houseTemplate.buildHouse();
    }
}
