package com.designpattern.practice;

import java.awt.*;

public class Client {

    public static void main(String[] args) {
        Component shape = new Circle();
        Decorator decorator = new BorderDecortor(new RedColorDecorator(shape));
        decorator.fillColor();
    }
}
