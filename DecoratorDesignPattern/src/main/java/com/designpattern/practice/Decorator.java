package com.designpattern.practice;

abstract class Decorator implements Component {
    Component component;
    public Decorator(Component component) {
        this.component = component;
    }

    @Override
    public void drawShape(){
        component.drawShape();
    }
    abstract  void fillColor();
}

class RedColorDecorator extends Decorator{

    public RedColorDecorator(Component component) {
        super(component);
    }

    @Override
    void fillColor() {
        super.drawShape();
        System.out.println("Fiied with Red Color");

    }
}

class BorderDecortor extends RedColorDecorator{

    public BorderDecortor(Component component) {
        super(component);
    }

    @Override
    void fillColor() {
        super.fillColor();
        System.out.println("Border style set to Italic width 3");
    }
}