package com.designpattern.practice;

interface Component {
    void drawShape();
}
class Circle implements Component{

    @Override
    public void drawShape(){
        System.out.println("Circle Drawn");
    }
}
class Triangle implements Component{

    @Override
    public void drawShape() {
        System.out.println("Triangle Drawn");
    }
}
