package com.designpattern.practice;

import java.util.LinkedList;
import java.util.List;

public class Supervisor implements Faculty{

    private String name;
    private String deptName;

    LinkedList<Faculty> facultyList = null;

    public Supervisor(String name, String deptName) {
        this.name = name;
        this.deptName = deptName;
        facultyList = new LinkedList<Faculty>();
    }

    public void addFaculty(Faculty faculty){
        facultyList.add(faculty);
    }
    public void removeFaculty(Faculty faculty){
        facultyList.remove(faculty);
    }
    @Override
    public String getDetails() {
        return toString();
    }
    public LinkedList<Faculty> getFaculty(){
       return facultyList;
    }

    @Override
    public String toString() {
        return "Supervisior{" +
                "name='" + name + '\'' +
                ", deptName='" + deptName + '\'' +
                '}';
    }
}
