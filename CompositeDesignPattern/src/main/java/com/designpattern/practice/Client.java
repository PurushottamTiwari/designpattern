package com.designpattern.practice;

import java.util.LinkedList;

public class Client {

    public static void main(String[] args) {


        Supervisor technologyDean = new Supervisor("Dr. Mike", "Dean of Technology");
        Supervisor chairOfMathDepartment = new Supervisor("Dr. John", "Chair of Math Department");
        Supervisor chairOfComputerScienceDepartment = new Supervisor("Dr. Ian", "Chair Of CS Department");

        FacultyMembers mathProf1 = new FacultyMembers("Math Professor1", "Adjunct", 302);
        FacultyMembers mathProf2 = new FacultyMembers("Math Professor2", "Associate", 303);

        FacultyMembers cseProf1 = new FacultyMembers("CSE Professor1", "Adjunct", 507);
        FacultyMembers cseProf2 = new FacultyMembers("CSE Professor2", "Professor", 508);
        FacultyMembers cseProf3 = new FacultyMembers("CSE Professor3", "Professor", 509);

        technologyDean.addFaculty(chairOfMathDepartment);
        technologyDean.addFaculty(chairOfComputerScienceDepartment);

        /* Professors of Mathematics directly reports to chair of math*/
        chairOfMathDepartment.addFaculty(mathProf1);
        chairOfMathDepartment.addFaculty(mathProf2);

        /*Professors of  Computer Sc. directly reports to chair of computer science*/
        chairOfComputerScienceDepartment.addFaculty(cseProf1);
        chairOfComputerScienceDepartment.addFaculty(cseProf2);
        chairOfComputerScienceDepartment.addFaculty(cseProf3);

        //Printing the details
        System.out.println("***COMPOSITE PATTERN DEMO ***");
        System.out.println("\nThe college has the following structure\n");

        technologyDean.getDetails();
        LinkedList<Faculty> chairs = technologyDean.getFaculty();

        for (int i = 0; i < chairs.size(); i++) {
            System.out.println(chairs.get(i).getDetails());
        }

        LinkedList<Faculty> mathProfessors = chairOfMathDepartment.getFaculty();
        for (int i = 0; i < mathProfessors.size(); i++) {
            System.out.println(mathProfessors.get(i).getDetails());
        }

        LinkedList<Faculty> cseProfessors = chairOfComputerScienceDepartment.getFaculty();
        for (int i = 0; i < cseProfessors.size(); i++) {
            System.out.println(cseProfessors.get(i).getDetails());
        }

        chairOfComputerScienceDepartment.removeFaculty(cseProf2);

        System.out.println("\n After CSE Professor2 leaving the organization- CSE department has following faculty:");

        cseProfessors = chairOfComputerScienceDepartment.getFaculty();
        for (int i = 0; i < cseProfessors.size(); i++) {
            System.out.println(cseProfessors.get(i).getDetails());
        }
    }
}
