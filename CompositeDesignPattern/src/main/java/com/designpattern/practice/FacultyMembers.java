package com.designpattern.practice;

import java.util.LinkedList;
import java.util.List;

interface Faculty{
    String getDetails();
}

class FacultyMembers implements Faculty {
    private String mName;
    private String mPosition;
    private int mOfficeNum;

    public FacultyMembers(String mName, String mPosition, int mOfficeNum) {
        this.mName = mName;
        this.mPosition = mPosition;
        this.mOfficeNum = mOfficeNum;
    }

    @Override
    public String  getDetails() {
        return toString();
    }

    @Override
    public String toString() {
        return "FacultyMembers{" +
                "mName='" + mName + '\'' +
                ", mPosition='" + mPosition + '\'' +
                ", mOfficeNum=" + mOfficeNum +
                '}';
    }
}

