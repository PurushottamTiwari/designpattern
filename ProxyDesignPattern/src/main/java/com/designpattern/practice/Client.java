package com.designpattern.practice;

public class Client {
    public static void main(String[] args) {
        InternetAccess internetAccess = new InternetProxy();
        internetAccess.getAccess("yahoo.com");
        internetAccess.getAccess("jetbrains.org");
    }
}
