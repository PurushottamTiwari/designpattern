package com.designpattern.practice;

import java.util.ArrayList;
import java.util.List;

public class InternetAccessImplementation implements InternetAccess{


    @Override
    public void getAccess(String siteName) {
       System.out.println("Connected to "+siteName);
    }
}
