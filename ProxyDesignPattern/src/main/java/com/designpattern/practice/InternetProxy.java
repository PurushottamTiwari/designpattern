package com.designpattern.practice;

import java.util.ArrayList;
import java.util.List;

public class InternetProxy implements InternetAccess{
    List<String> siteList = new ArrayList<>();
    InternetAccess internetAccess;

    public InternetProxy() {
       populateList();
    }

    public void populateList(){
        siteList.add("google.com");
        siteList.add("hotmail.com");
        siteList.add("jetbrains.org");
    }
    @Override
    public void getAccess(String siteName) {
        if(siteList.contains(siteName)){
            internetAccess = new InternetAccessImplementation();
            internetAccess.getAccess(siteName);
        }else{
            System.out.println("Access Denied : Restricted Site");
        }
    }
}
