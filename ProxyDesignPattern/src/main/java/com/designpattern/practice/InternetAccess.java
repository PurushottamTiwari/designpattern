package com.designpattern.practice;

public interface InternetAccess {

    void getAccess(String siteName);
}
