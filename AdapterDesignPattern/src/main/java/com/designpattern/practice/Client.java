package com.designpattern.practice;

import javax.print.attribute.standard.Media;

public class Client {

    public static void main(String[] args) {
        MediaPlayer mediaPlayer = new AudioPlayer();
        mediaPlayer.play(".mp3","song1");

        AdvancedMediaPlayer advancedMediaPlayer = new vlcPlayer();

        MediaPlayer mediaPlayer1 = new MediaPlayerAdapter(advancedMediaPlayer);
        mediaPlayer1.play("vlc","song2");


         advancedMediaPlayer = new MP4();

         mediaPlayer1 = new MediaPlayerAdapter(advancedMediaPlayer);
        mediaPlayer1.play("vlc","song3");
    }
}
