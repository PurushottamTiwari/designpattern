package com.designpattern.practice;

interface MediaPlayer {
    void play(String audioType, String fileName);
}
class AudioPlayer implements MediaPlayer{

    @Override
    public void play(String audioType, String fileName) {
        System.out.println("Now Playing"+fileName+audioType);
    }
}
