package com.designpattern.practice;

interface AdvancedMediaPlayer {
    void loadFileName(String fileName);
    void listen();
}
class vlcPlayer implements AdvancedMediaPlayer{
 String fileName = null;
    @Override
    public void loadFileName(String fileName) {
     this.fileName = fileName;
    }

    @Override
    public void listen() {
      System.out.println("Playing "+fileName+".vlc");
    }
}

class MP4 implements AdvancedMediaPlayer{
    String fileName = null;
    @Override
    public void loadFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void listen() {
        System.out.println("Playing "+fileName+".mp4");
    }
}
