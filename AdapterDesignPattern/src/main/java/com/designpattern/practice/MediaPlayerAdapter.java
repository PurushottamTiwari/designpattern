package com.designpattern.practice;

class  MediaPlayerAdapter implements MediaPlayer{
  AdvancedMediaPlayer advancedMediaPlayer;

    public MediaPlayerAdapter(AdvancedMediaPlayer advancedMediaPlayer) {
        this.advancedMediaPlayer = advancedMediaPlayer;
    }

    @Override
    public void play(String audioType, String fileName) {

            advancedMediaPlayer.loadFileName(fileName);
            advancedMediaPlayer.listen();
    }
}
