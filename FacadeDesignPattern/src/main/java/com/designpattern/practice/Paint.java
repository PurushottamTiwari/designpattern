package com.designpattern.practice;

public class Paint {

    WhiteCar whiteCar;

    public Paint(WhiteCar whiteCar) {
        this.whiteCar = whiteCar;
    }

    public void paintCar(){
        whiteCar.setPaintCar("Car painted with white color");
    }

}
