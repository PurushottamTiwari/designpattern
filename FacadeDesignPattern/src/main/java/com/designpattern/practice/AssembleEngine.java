package com.designpattern.practice;

public class AssembleEngine {

    WhiteCar whiteCar;

    public AssembleEngine(WhiteCar whiteCar) {
        this.whiteCar = whiteCar;
    }

    public void assembleEngine(){
        whiteCar.setAssembleEngine("Car Engine has been assembled");
    }

}
