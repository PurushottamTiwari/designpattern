package com.designpattern.practice;

public class CarFacade implements Car{
    @Override
    public WhiteCar manufactureCar() {
          WhiteCar whiteCar = new WhiteCar();

          AssembleBody assembleBody = new AssembleBody(whiteCar);
          assembleBody.assembleBody();

          AssembleEngine assembleEngine = new AssembleEngine(whiteCar);
          assembleEngine.assembleEngine();

          AttachWheels attachWheels = new AttachWheels(whiteCar);
          attachWheels.assembleWheels();

          Paint paint = new Paint(whiteCar);
          paint.paintCar();
          return whiteCar;
    }
}
