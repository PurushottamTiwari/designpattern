package com.designpattern.practice;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class WhiteCar {
    String assembleBody;
    String assembleEngine;
    String attachWheels;
    String paintCar;
}
