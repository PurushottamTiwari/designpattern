package com.designpattern.practice;

public class Client {
    public static void main(String[] args) {
        Car car = new CarFacade();
        System.out.println(car.manufactureCar());
    }
}
