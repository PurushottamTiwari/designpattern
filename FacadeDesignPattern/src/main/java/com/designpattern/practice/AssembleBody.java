package com.designpattern.practice;

public class AssembleBody {
  WhiteCar  whiteCar;

   public AssembleBody(WhiteCar whiteCar) {
      this.whiteCar = whiteCar;
   }

   public void assembleBody(){

      whiteCar.setAssembleBody("Car body has been assembled");
   }

}
