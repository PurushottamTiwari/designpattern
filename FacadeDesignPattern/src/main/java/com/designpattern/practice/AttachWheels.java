package com.designpattern.practice;

public class AttachWheels {
    WhiteCar whiteCar;

    public AttachWheels(WhiteCar whiteCar) {
        this.whiteCar = whiteCar;
    }

    public void assembleWheels(){
        whiteCar.setAttachWheels("Attached Four Wheels");
    }

}
