package com.designpattern.practice;

import java.util.ArrayList;
import java.util.List;

public interface Mediator {
    void addUser(User user);
    void sendMessage(String message, User sender);
}

class MessageMediator implements Mediator{
    List<User> userList ;

    public MessageMediator() {
        this.userList = new ArrayList<>();
    }

    @Override
    public void addUser(User user) {
        userList.add(user);
    }

    @Override
    public void sendMessage(String message, User sender) {
        for (User u : userList) {
            if (u != sender) u.receiveMessage(message);
        }

    }
}

