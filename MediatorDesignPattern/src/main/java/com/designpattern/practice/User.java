package com.designpattern.practice;

abstract class User {
    protected Mediator mediator;
    protected String name;

    public User(Mediator mediator, String name) {
        this.mediator = mediator;
        this.name = name;
    }

    abstract void sendMessage(String msg);
    abstract void receiveMessage(String msg);
}

class UserImpl  extends User{

    public UserImpl(Mediator mediator, String name) {
        super(mediator,name);

    }

    @Override
    void sendMessage(String msg) {
        mediator.sendMessage(msg,this);
    }

    @Override
    void receiveMessage(String msg) {
        System.out.println(this.name +"-"+msg);
    }
}
