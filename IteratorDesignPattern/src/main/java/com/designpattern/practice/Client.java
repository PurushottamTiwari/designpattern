package com.designpattern.practice;

public class Client {
    public static void main(String[] args) {
        NotificationCollection notificationCollection = new NotificationCollection();
        notificationCollection.addNotificatio(new Notification("Notification1"));
        notificationCollection.addNotificatio(new Notification("notification2"));
        notificationCollection.addNotificatio(new Notification("notification3"));

        Iterator iterator = notificationCollection.getIterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next().getNotification());
        }
    }
}
