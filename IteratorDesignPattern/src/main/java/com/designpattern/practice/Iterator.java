package com.designpattern.practice;

import java.util.List;

public interface Iterator {

    public boolean hasNext();
    public Notification next();

}
class NotificationIterator implements Iterator{

  private Notification notification;
  private int currentPosition = 0 ;
  private List<Notification> notifications;

    public NotificationIterator(List<Notification> notifications) {
        this.notifications = notifications;
    }


    @Override
    public boolean hasNext() {
        if(currentPosition >= notifications.size() || notifications.get(currentPosition) == null)return false;
        else return true;
    }

    @Override
    public Notification next() {
        notification =  notifications.get(currentPosition);
        currentPosition += 1;
        return notification;
    }
}