package com.designpattern.practice;

import java.util.ArrayList;
import java.util.List;

public class NotificationCollection {

    private List<Notification> notifications = null;
    private Iterator iterator = null;

    public NotificationCollection(List<Notification> notifications) {
        this.notifications = notifications;
    }

    public NotificationCollection() {
        notifications = new ArrayList<>();
    }

    public void addNotificatio(Notification notification){
        notifications.add(notification);
    }
    public void removeNotification(Notification notification){
        notifications.remove(notification);
    }

    public Iterator getIterator(){
        return new NotificationIterator(notifications);
    }
}
