package com.designpatter.practice;

public class Client {
    public static void main(String[] args) {
        Stock stock = new Stock("Google", 100);

        StockReceiver stockReceiver = new StockReceiver();

        Order order = new BuyStock(stockReceiver);

        Broker broker = new Broker(order , stock);

        broker.execute();

         order = new SellStock(stockReceiver);

         broker = new Broker(order , stock);

         broker.execute();
    }
}
