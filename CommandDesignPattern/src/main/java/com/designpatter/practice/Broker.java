package com.designpatter.practice;

public class Broker {
    Order order;
    Stock stock;

    public Broker(Order order, Stock stock) {
        this.order = order;
        this.stock = stock;
    }

    public void execute(){
        order.execute(stock);
    }
}
