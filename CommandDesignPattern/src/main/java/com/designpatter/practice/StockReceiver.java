package com.designpatter.practice;


class  StockReceiver{

    public void buyStock(Stock stock) {
     System.out.println("["+stock.getStockName()+","+stock.getQuantity()+"]"+"bought");
    }

    public void sellStock(Stock stock) {
        System.out.println("["+stock.getStockName()+","+stock.getQuantity()+"]"+"sold");
    }
}

