package com.designpatter.practice;

public interface Order {

    void execute(Stock stock);

}
class BuyStock implements Order{
   StockReceiver stockReceiver;

    public BuyStock(StockReceiver stockReceiver) {
        this.stockReceiver = stockReceiver;
    }

    @Override
    public void execute(Stock stock) {
        stockReceiver.buyStock(stock);
    }
}
class SellStock implements Order{
    StockReceiver stockReceiver;

    public SellStock(StockReceiver stockReceiver) {
        this.stockReceiver = stockReceiver;
    }

    @Override
    public void execute(Stock stock) {
        stockReceiver.sellStock(stock);
    }
}