package com.designpattern.practice;

public class Client {
    public static void main(String[] args) {
        Originator originator = new Originator();
        originator.appendText("Hi\n");
        originator.appendText("Everybody\n");
        CareTaker careTaker = new CareTaker(originator);
        System.out.println(originator);
        careTaker.saveState();
        originator.appendText("Welcome to DesignPattern\n");
        careTaker.restoreState();
        System.out.println(originator);
    }
}
