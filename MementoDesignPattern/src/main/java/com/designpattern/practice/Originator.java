package com.designpattern.practice;

public class Originator {

    private StringBuilder text;

    public Originator() {
        this.text = new StringBuilder();
    }

    public void appendText(String text){
      this.text.append(text);
    }
    public String showText(){
        return this.text.toString();
    }

    @Override
    public String toString() {
        return "Originator{" +
                "text=" + text +
                '}';
    }

    public Object SaveState(){
       return new Memento (this.text);
    }
    public void restoreState(Object memento){
        this.text = ((Memento)memento).text;
    }
    private class Memento{
        private StringBuilder text;


        public Memento(StringBuilder text) {
            this.text = new StringBuilder(text);

        }
    }
}
