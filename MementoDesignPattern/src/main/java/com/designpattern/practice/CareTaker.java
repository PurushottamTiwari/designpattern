package com.designpattern.practice;

public class CareTaker {
    private Originator originator;
    private Object memento;

    public CareTaker(Originator originator) {
        this.originator = originator;
    }

    public void saveState(){
        memento = originator.SaveState();
    }

    public void restoreState(){
        originator.restoreState(this.memento);
    }
}
