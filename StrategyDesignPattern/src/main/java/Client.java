public class Client {
    public static void main(String[] args) {
        Item item1 = new Item("ABC",2.0);
        Item item2 = new Item("XYZ", 32.32);
        Item item3 = new Item("A123",45.32);

        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addItemToCart(item1);
        shoppingCart.addItemToCart(item2);
        shoppingCart.addItemToCart(item3);

        shoppingCart.payTotal(new PaymentByCard(9503684915l,123,"12/15","ABC"));
    }
}
