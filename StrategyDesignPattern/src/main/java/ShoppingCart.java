import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
    List<Item> itemList ;

    public ShoppingCart() {
        this.itemList = new ArrayList<>();
    }

    public void addItemToCart(Item item){
        itemList.add(item);
    }
    public void removeItemToCart(Item item){
        itemList.remove(item);
    }
    private double calculateTotal(){
        double sum = 0.0;
        for(Item item :itemList)
            sum += item.getPrice();
        return sum;
    }

    public void payTotal(PaymentStrategy paymentStrategy){
        double total = calculateTotal();
        paymentStrategy.pay(total);
    }
}
