interface PaymentStrategy {
    public void pay(double amount);
}
class PaymentByCard implements PaymentStrategy{
    private  long cardNumber;
    private int cvv;
    private String expiryDate;
    private String cardHolderName;

    public PaymentByCard(long cardNumber, int cvv, String expiryDate, String cardHolderName) {
        this.cardNumber = cardNumber;
        this.cvv = cvv;
        this.expiryDate = expiryDate;
        this.cardHolderName = cardHolderName;
    }

    @Override
    public void pay(double amount) {
        System.out.println(amount+"$"+"Paid by Credit/Debit Card");
    }
}

class PaymentByGooglePay implements PaymentStrategy{

    private long mobileNumber;

    public PaymentByGooglePay(long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Override
    public void pay(double amount) {
        System.out.println(amount+"$"+"Paid by Google Pay");
    }
}
