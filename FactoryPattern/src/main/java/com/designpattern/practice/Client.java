package com.designpattern.practice;

public class Client {
    public static void main(String[] args) {
        NotificationFcatory notificationFcatory = new NotificationFactoryImpl();
        Notification notification = null ;

        notification = notificationFcatory.send("SMS");
        notification.sendNotification();

        notification = notificationFcatory.send("Email");
        notification.sendNotification();


        notification = notificationFcatory.send("Push");
        notification.sendNotification();
    }
}
