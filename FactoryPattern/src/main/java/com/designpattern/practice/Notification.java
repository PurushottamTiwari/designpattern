package com.designpattern.practice;

interface Notification {
   void sendNotification();
}

class SMSNotification implements Notification{
	public void sendNotification() {
		System.out.println("Notification send through SMS");
	}
}
class EmailNotification implements Notification{
	@Override
	public void sendNotification() {
		System.out.println("Notification send through Email");
	}
}
class PushNotification implements Notification{

	@Override
	public void sendNotification() {
		System.out.println("Notification send through Push Notification");
	}
}