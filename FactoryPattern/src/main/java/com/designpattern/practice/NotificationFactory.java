package com.designpattern.practice;

interface NotificationFcatory {
    Notification send(String notificationType);
}

class NotificationFactoryImpl implements NotificationFcatory {

    Notification notification;

    @Override
    public Notification send(String notificationType) {
        if (notificationType == null || "".equalsIgnoreCase(notificationType)) return null;
        switch (notificationType) {
            case "SMS":
                return new SMSNotification();

            case "Email":
                return new EmailNotification();

            case "Push":
                return new PushNotification();

            default:
                return null;
        }
    }
}
