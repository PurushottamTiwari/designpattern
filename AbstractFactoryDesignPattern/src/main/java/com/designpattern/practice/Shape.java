package com.designpattern.practice;

public interface Shape {
    void createShape();
}

class Circle implements Shape {
    @Override
    public void createShape() {
        System.out.println("Circle Created");
    }
}

class Rectangle implements Shape {
    @Override
    public void createShape() {
        System.out.println("Rectangle Created");
    }
}

class Square implements Shape {
    @Override
    public void createShape() {
        System.out.println("Square Created");
    }
}
