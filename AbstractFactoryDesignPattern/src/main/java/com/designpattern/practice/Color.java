package com.designpattern.practice;

interface Color {
    void setColor();
}
class Red implements Color{

    @Override
    public void setColor() {
        System.out.println("Red color set");
    }
}
class Green implements Color{

    @Override
    public void setColor() {
        System.out.println("Green color set");
    }
}
class Blue implements Color{

    @Override
    public void setColor() {
        System.out.println("Blue color set");
    }
}
