package com.designpattern.practice;

class FactoryProducer{
    public static  AbstractFactory getFcatory(String factoryType) {
        switch (factoryType){
            case "shape":
                return new ShapeFactory();
            case "color":
                return new ColorFactory();

        }
        return null;
    }
}
