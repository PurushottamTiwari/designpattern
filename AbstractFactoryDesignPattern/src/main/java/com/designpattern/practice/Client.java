package com.designpattern.practice;

public class Client {

    public static void main(String[] args) {
        AbstractFactory abstractShapeFactory = FactoryProducer.getFcatory("shape");

        abstractShapeFactory.createShape("Circle").createShape();
        abstractShapeFactory.createShape("Square").createShape();
        abstractShapeFactory.createShape("Rectangle").createShape();

        AbstractFactory abstractColorFactory = FactoryProducer.getFcatory("color");
        abstractColorFactory.fillColor("Red").setColor();
        abstractColorFactory.fillColor("Green").setColor();
        abstractColorFactory.fillColor("Blue").setColor();
    }
}
