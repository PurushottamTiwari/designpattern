package com.designpattern.practice;

interface AbstractFactory {
    default Shape createShape(String shape) {
        return null;
    }

    default Color fillColor(String color) {
        return null;
    }
}

class ShapeFactory implements AbstractFactory {
  Shape shape = null;
    @Override
    public Shape createShape(String shape) {
        if(shape == null || "".equalsIgnoreCase(shape))return null;
        switch(shape){
            case "Rectangle":
                return new Rectangle();
            case "Circle":
                return new Circle();
            case "Square":
                return new Square();
            default : return null;
        }
    }
}

class ColorFactory implements AbstractFactory {
    Color color;
    @Override
    public Color fillColor(String color) {
        if(color == null || "".equalsIgnoreCase(color)) return null;
        switch(color){
            case "Red":
                return new Red();
            case "Green":
                return new Green();
            case "Blue":
                return new Blue();
            default : return null;
        }
    }
}