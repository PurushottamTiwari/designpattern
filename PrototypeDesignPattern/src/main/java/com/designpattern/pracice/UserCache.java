package com.designpattern.pracice;

import java.util.Hashtable;

public class UserCache {
    static Hashtable<String , User> USER_CACHE = new Hashtable<String , User>();

    static{
        USER_CACHE.put("1",new User("1","P","T"));
        USER_CACHE.put("2",new User("2","N","T"));
        USER_CACHE.put("3", new User("3","M","T"));
    }

   public static Object  getUser(String uerId){
        if(!USER_CACHE.containsKey(uerId))return null;
        User user = USER_CACHE.get(uerId);
        System.out.println(user.hashCode());
        return user.clone();
   }
}
