package com.designpattern.practice;

public class Client {
    public static void main(String[] args) {
        Queue subject = new Queue();

        Observer observer1 = new QueueReceiver(subject,"OBS1");
        Observer observer2 = new QueueReceiver(subject,"OBS2");
        Observer observer3 = new QueueReceiver(subject,"OBS3");

        subject.addObserver(observer1);
        subject.addObserver(observer2);
        subject.addObserver(observer3);

        subject.postMessage("Hello World");
    }
}
