package com.designpattern.practice;

import java.util.ArrayList;
import java.util.List;

interface Subject {

    void addObserver(Observer Observer);
    void removeObserver(Observer observer);
    void notifyObservers();
}

class Queue implements Subject{
    String message;
    boolean changed;

    List<Observer> observerList = new ArrayList<>();
    @Override
    public void addObserver(Observer Observer) {
        if(Observer == null)return;
        observerList.add(Observer);
    }

    @Override
    public void removeObserver(Observer observer) {
      observerList.remove(observer);
    }

    @Override
    public void notifyObservers() {
        if(!changed)return;
        changed = false;
      for(Observer observer :observerList){
          observer.receiveMessage(message);
      }
    }

    public void postMessage(String message){
        this.message = message;
        this.changed = true;
        notifyObservers();
    }

}