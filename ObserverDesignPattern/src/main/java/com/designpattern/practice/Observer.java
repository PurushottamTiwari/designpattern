package com.designpattern.practice;

public interface Observer {
    void receiveMessage(String msg);

}
class QueueReceiver implements Observer{

    Subject subject;
    String name;

    public QueueReceiver(Subject subject, String name) {
        this.subject = subject;
        this.name = name;
    }

    @Override
    public void receiveMessage(String msg) {
        System.out.println(this.name+"Got Message :"+msg);
    }



}
