public interface EmployeeVisitor {
    public Employee visit(EmployeeImpl employee);
}

class EmployeeVisitorImpl implements EmployeeVisitor{

    @Override
    public Employee visit(EmployeeImpl employee) {
         employee.setEmployeeName("Mr. " +employee.getEmployeeName());
         return employee;
    }
}