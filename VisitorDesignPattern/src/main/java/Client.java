import java.util.ArrayList;
import java.util.List;

public class Client {
      private static void  addSalutation(List<Employee> employeeList){
        EmployeeVisitor employeeVisitor = new EmployeeVisitorImpl();
        for(Employee employee : employeeList){
            employee.accept(employeeVisitor);
        }
      }


    public static void main(String[] args) {
        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(new EmployeeImpl("Purushottam Tiwari"));
        employeeList.add(new EmployeeImpl("Mukesh Tiwari"));
        employeeList.add(new EmployeeImpl("Mukund Tiwari"));
        for(Employee employee : employeeList){
            System.out.println(employee);
        }
        addSalutation(employeeList);
        for(Employee employee : employeeList){
            System.out.println(employee);
        }

    }
}
