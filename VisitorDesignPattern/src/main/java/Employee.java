abstract class Employee {

    public abstract void accept(EmployeeVisitor employeeVisitor);

}

class EmployeeImpl extends Employee{
    private String  employeeName;

    public EmployeeImpl(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    @Override
    public void accept(EmployeeVisitor employeeVisitor) {
        employeeVisitor.visit(this);
    }

    @Override
    public String toString() {
        return "EmployeeImpl{" +
                "employeeName='" + employeeName + '\'' +
                '}';
    }
}